// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Space Pizza`
  String get app_name {
    return Intl.message(
      'Space Pizza',
      name: 'app_name',
      desc: '',
      args: [],
    );
  }

  /// `Registration`
  String get registration_title {
    return Intl.message(
      'Registration',
      name: 'registration_title',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get registration_button {
    return Intl.message(
      'Register',
      name: 'registration_button',
      desc: '',
      args: [],
    );
  }

  /// `Sign-in`
  String get sign_in_title {
    return Intl.message(
      'Sign-in',
      name: 'sign_in_title',
      desc: '',
      args: [],
    );
  }

  /// `Sign-in`
  String get sign_in_button {
    return Intl.message(
      'Sign-in',
      name: 'sign_in_button',
      desc: '',
      args: [],
    );
  }

  /// `Enter your email`
  String get email_input_hint {
    return Intl.message(
      'Enter your email',
      name: 'email_input_hint',
      desc: '',
      args: [],
    );
  }

  /// `Enter your password`
  String get password_input_hint {
    return Intl.message(
      'Enter your password',
      name: 'password_input_hint',
      desc: '',
      args: [],
    );
  }

  /// `Sorry, Firebase initialization error`
  String get error_firebase_init {
    return Intl.message(
      'Sorry, Firebase initialization error',
      name: 'error_firebase_init',
      desc: '',
      args: [],
    );
  }

  /// `The password provided is too weak`
  String get error_auth_weak_password {
    return Intl.message(
      'The password provided is too weak',
      name: 'error_auth_weak_password',
      desc: '',
      args: [],
    );
  }

  /// `The account already exists for that email`
  String get error_auth_account_exists {
    return Intl.message(
      'The account already exists for that email',
      name: 'error_auth_account_exists',
      desc: '',
      args: [],
    );
  }

  /// `No user found for that email`
  String get error_auth_user_not_found {
    return Intl.message(
      'No user found for that email',
      name: 'error_auth_user_not_found',
      desc: '',
      args: [],
    );
  }

  /// `Wrong password provided for that user`
  String get error_auth_wrong_password {
    return Intl.message(
      'Wrong password provided for that user',
      name: 'error_auth_wrong_password',
      desc: '',
      args: [],
    );
  }

  /// `Orders`
  String get free_orders_title {
    return Intl.message(
      'Orders',
      name: 'free_orders_title',
      desc: '',
      args: [],
    );
  }

  /// `My orders`
  String get my_orders_title {
    return Intl.message(
      'My orders',
      name: 'my_orders_title',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings_title {
    return Intl.message(
      'Settings',
      name: 'settings_title',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get about_title {
    return Intl.message(
      'About',
      name: 'about_title',
      desc: '',
      args: [],
    );
  }

  /// `We are aliens who make extraterrestrial food. Our mission is to feed the entire planet. Our contacts: 26,000 light years from the center of the galaxy`
  String get about_content {
    return Intl.message(
      'We are aliens who make extraterrestrial food. Our mission is to feed the entire planet. Our contacts: 26,000 light years from the center of the galaxy',
      name: 'about_content',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}