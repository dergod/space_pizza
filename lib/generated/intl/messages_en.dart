// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "about_content" : MessageLookupByLibrary.simpleMessage("We are aliens who make extraterrestrial food. Our mission is to feed the entire planet. Our contacts: 26,000 light years from the center of the galaxy"),
    "about_title" : MessageLookupByLibrary.simpleMessage("About"),
    "app_name" : MessageLookupByLibrary.simpleMessage("Space Pizza"),
    "email_input_hint" : MessageLookupByLibrary.simpleMessage("Enter your email"),
    "error_auth_account_exists" : MessageLookupByLibrary.simpleMessage("The account already exists for that email"),
    "error_auth_user_not_found" : MessageLookupByLibrary.simpleMessage("No user found for that email"),
    "error_auth_weak_password" : MessageLookupByLibrary.simpleMessage("The password provided is too weak"),
    "error_auth_wrong_password" : MessageLookupByLibrary.simpleMessage("Wrong password provided for that user"),
    "error_firebase_init" : MessageLookupByLibrary.simpleMessage("Sorry, Firebase initialization error"),
    "free_orders_title" : MessageLookupByLibrary.simpleMessage("Orders"),
    "my_orders_title" : MessageLookupByLibrary.simpleMessage("My orders"),
    "password_input_hint" : MessageLookupByLibrary.simpleMessage("Enter your password"),
    "registration_button" : MessageLookupByLibrary.simpleMessage("Register"),
    "registration_title" : MessageLookupByLibrary.simpleMessage("Registration"),
    "settings_title" : MessageLookupByLibrary.simpleMessage("Settings"),
    "sign_in_button" : MessageLookupByLibrary.simpleMessage("Sign-in"),
    "sign_in_title" : MessageLookupByLibrary.simpleMessage("Sign-in")
  };
}
