// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "about_content" : MessageLookupByLibrary.simpleMessage("Мы инопланетяне, делающие неземную еду. Наша миссия - накормить планету. Наши контакты: 26,000 световых лет от центра галактики"),
    "about_title" : MessageLookupByLibrary.simpleMessage("О нас"),
    "app_name" : MessageLookupByLibrary.simpleMessage("Space Pizza"),
    "email_input_hint" : MessageLookupByLibrary.simpleMessage("Введите email"),
    "error_auth_account_exists" : MessageLookupByLibrary.simpleMessage("Аккаунт уже существует"),
    "error_auth_user_not_found" : MessageLookupByLibrary.simpleMessage("Не найдено пользователя с таким email"),
    "error_auth_weak_password" : MessageLookupByLibrary.simpleMessage("Придуманный пароль слишком слаб"),
    "error_auth_wrong_password" : MessageLookupByLibrary.simpleMessage("Неверный пароль"),
    "error_firebase_init" : MessageLookupByLibrary.simpleMessage("Извините, ошибка инициализации Firebase сервисов"),
    "free_orders_title" : MessageLookupByLibrary.simpleMessage("Заказы"),
    "my_orders_title" : MessageLookupByLibrary.simpleMessage("Мои заказы"),
    "password_input_hint" : MessageLookupByLibrary.simpleMessage("Введите пароль"),
    "registration_button" : MessageLookupByLibrary.simpleMessage("Зарегистрироваться"),
    "registration_title" : MessageLookupByLibrary.simpleMessage("Регистрация"),
    "settings_title" : MessageLookupByLibrary.simpleMessage("Настройки"),
    "sign_in_button" : MessageLookupByLibrary.simpleMessage("Войти"),
    "sign_in_title" : MessageLookupByLibrary.simpleMessage("Авторизация")
  };
}
