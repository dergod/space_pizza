import 'package:flutter/material.dart';

/// Common widget for indicating progress state
class ProgressWidget extends StatelessWidget {
  final _progressSize = 32.0;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            child: CircularProgressIndicator(),
            width: _progressSize,
            height: _progressSize));
  }
}
