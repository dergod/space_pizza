import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:space_pizza/di/get_it.dart';
import 'package:space_pizza/generated/l10n.dart';
import 'package:space_pizza/ui/firebase/firebase_widget.dart';

/// Main method
void main() {
  setupDi();
  runApp(SpacePizzaApp());
}

/// Main widget
class SpacePizzaApp extends StatelessWidget {
  final _primaryColor = Color(0xff81d4fa);
  final _primaryColorLight = Color(0xffb6ffff);
  final _primaryColorDark = Color(0xff4ba3c7);
  final _accentColor = Color(0xffb0bec5);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: _primaryColor,
          primaryColorLight: _primaryColorLight,
          primaryColorDark: _primaryColorDark,
          accentColor: _accentColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: S.delegate.supportedLocales,
        onGenerateTitle: (BuildContext context) => S.of(context).app_name,
        home: FirebaseWidget());
  }
}
