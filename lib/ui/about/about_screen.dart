import 'package:flutter/material.dart';

import 'package:space_pizza/dimens.dart';
import 'package:space_pizza/generated/l10n.dart';

/// Screen with the company description
class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(Dimens.medium_gap),
        child: SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: Center(
              child: Text(S.of(context).about_content,
                  softWrap: true,
                  style: TextStyle(fontSize: Dimens.extra_medium_gap)),
            )));
  }
}
