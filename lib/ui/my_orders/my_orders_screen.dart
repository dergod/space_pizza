import 'package:flutter/material.dart';

import 'package:space_pizza/generated/l10n.dart';

class MyOrdersScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text(S.of(context).my_orders_title));
  }
}
