import 'package:flutter/material.dart';

import 'package:space_pizza/generated/l10n.dart';

class FreeOrdersScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text(S.of(context).free_orders_title));
  }
}
