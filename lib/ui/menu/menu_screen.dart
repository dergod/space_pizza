import 'package:flutter/material.dart';
import 'package:space_pizza/generated/l10n.dart';

import 'package:space_pizza/ui/about/about_screen.dart';
import 'package:space_pizza/ui/free_orders/free_orders_screen.dart';
import 'package:space_pizza/ui/my_orders/my_orders_screen.dart';
import 'package:space_pizza/ui/settings/settings_screen.dart';

/// Main screen with bottom navigation
class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  static List<Widget> _menuItems = <Widget>[
    FreeOrdersScreen(),
    MyOrdersScreen(),
    SettingsScreen(),
    AboutScreen()
  ];

  int _selectedMenuItem = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedMenuItem = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(child: _menuItems.elementAt(_selectedMenuItem)),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list_outlined),
            label: S.of(context).free_orders_title,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bookmarks_outlined),
            label: S.of(context).my_orders_title,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings_outlined),
            label: S.of(context).settings_title,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.info_outlined),
            label: S.of(context).about_title,
          )
        ],
        currentIndex: _selectedMenuItem,
        selectedItemColor: Theme.of(context).accentColor,
        onTap: _onItemTapped,
      ),
    );
  }
}
