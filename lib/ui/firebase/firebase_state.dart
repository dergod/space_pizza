/// State for Firebase initializing result
enum FirebaseState { success, error, in_progress }
