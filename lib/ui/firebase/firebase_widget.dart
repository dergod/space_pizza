import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:space_pizza/di/get_it.dart';
import 'package:space_pizza/generated/l10n.dart';

import 'package:space_pizza/ui/auth/auth_widget.dart';
import 'package:space_pizza/ui/common/progress_widget.dart';
import 'package:space_pizza/ui/firebase/firebase_state.dart';

/// Widget for initializing Firebase
class FirebaseWidget extends StatefulWidget {
  _FirebaseWidgetState createState() => _FirebaseWidgetState();
}

class _FirebaseWidgetState extends State<FirebaseWidget> {
  FirebaseState _firebaseState = FirebaseState.in_progress;

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  void initializeFlutterFire() async {
    try {
      await getIt.getAsync<FirebaseApp>();
      setState(() {
        _firebaseState = FirebaseState.success;
      });
    } catch (e) {
      setState(() {
        _firebaseState = FirebaseState.error;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    switch (_firebaseState) {
      case FirebaseState.in_progress:
        {
          return ProgressWidget();
        }

      case FirebaseState.error:
        {
          return Center(
              child: Container(child: Text(S.of(context).error_firebase_init)));
        }

      default:
        {
          return AuthWidget();
        }
    }
  }
}
