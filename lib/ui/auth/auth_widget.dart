import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:space_pizza/di/get_it.dart';
import 'package:space_pizza/ui/auth/sign_in_screen.dart';
import 'package:space_pizza/ui/common/progress_widget.dart';
import 'package:space_pizza/ui/menu/menu_screen.dart';

const prefsKeyIsAuthorized = 'authorized';

/// Authorization state widget
class AuthWidget extends StatefulWidget {
  @override
  _AuthWidgetState createState() => _AuthWidgetState();
}

class _AuthWidgetState extends State<AuthWidget> {
  bool _isInProgress = true;
  bool _isAuthorized = false;
  StreamSubscription _localAuthStateSubscription;
  StreamSubscription _remoteAuthStateSubscription;

  @override
  void initState() {
    super.initState();
    _localAuthStateSubscription =
        _getLocalAuthState().asStream().listen((bool isAuthorized) {
      if (isAuthorized) {
        setState(() {
          _isInProgress = false;
          _isAuthorized = true;
        });
      } else {
        _listenRemoteAuthState();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isInProgress) {
      return ProgressWidget();
    } else {
      if (_isAuthorized) {
        return MenuScreen();
      } else {
        return SignInScreen();
      }
    }
  }

  /// Get saved state from preferences
  Future<bool> _getLocalAuthState() async {
    SharedPreferences prefs = await getIt.getAsync<SharedPreferences>();
    return prefs.getBool(prefsKeyIsAuthorized) ?? false;
  }

  /// Get state from Firebase service
  void _listenRemoteAuthState() {
    _remoteAuthStateSubscription =
        getIt.get<FirebaseAuth>().authStateChanges().listen((User user) {
      if (user == null) {
        setState(() {
          _isInProgress = false;
          _isAuthorized = false;
        });
      } else {
        setState(() {
          _isInProgress = false;
          _isAuthorized = true;
        });
      }
    });
  }

  @override
  void dispose() {
    _localAuthStateSubscription.cancel();
    _remoteAuthStateSubscription.cancel();
    super.dispose();
  }
}
