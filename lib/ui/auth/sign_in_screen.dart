import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:space_pizza/di/get_it.dart';
import 'package:space_pizza/generated/l10n.dart';
import 'package:space_pizza/ui/auth/auth_widget.dart';
import 'package:space_pizza/ui/auth/registration_screen.dart';
import 'package:space_pizza/dimens.dart';
import 'package:space_pizza/ui/menu/menu_screen.dart';

const errorCodeUserNotFound = 'user-not-found';
const errorCodeWrongPassword = 'wrong-password';

/// Widget for user signing-in
class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  TextEditingController _emailController;
  FocusNode _emailFocusNode;
  TextEditingController _pswdController;
  FocusNode _passwordFocusNode;

  @override
  void initState() {
    super.initState();
    _emailController =
        TextEditingController(text: S.of(context).email_input_hint);
    _emailFocusNode = FocusNode();
    _emailFocusNode.addListener(() {
      if (_emailFocusNode.hasFocus) _emailController.clear();
    });
    _pswdController =
        TextEditingController(text: S.of(context).password_input_hint);
    _passwordFocusNode = FocusNode();
    _passwordFocusNode.addListener(() {
      if (_passwordFocusNode.hasFocus) _pswdController.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(S.of(context).sign_in_title)),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _emailInput(),
              _passwordInput(),
              _signInButton(),
              _registrationButton()
            ]));
  }

  Widget _emailInput() {
    return Container(
        margin: EdgeInsets.symmetric(
            horizontal: Dimens.medium_gap, vertical: Dimens.small_gap),
        child: TextField(
            controller: _emailController,
            focusNode: _emailFocusNode,
            keyboardType: TextInputType.emailAddress));
  }

  Widget _passwordInput() {
    return Container(
        margin: EdgeInsets.symmetric(
            horizontal: Dimens.medium_gap, vertical: Dimens.small_gap),
        child: TextField(
            controller: _pswdController,
            focusNode: _passwordFocusNode,
            keyboardType: TextInputType.visiblePassword));
  }

  Widget _signInButton() {
    return SizedBox(
      width: double.infinity,
      child: Container(
          margin: EdgeInsets.only(
              left: Dimens.medium_gap,
              top: Dimens.large_gap,
              right: Dimens.medium_gap,
              bottom: Dimens.small_gap),
          child: ElevatedButton(
              onPressed: () async {
                final signInResult = await _signIn();
                if (signInResult) _navigateToMain();
              },
              child: Text(S.of(context).sign_in_button,
                  style: TextStyle(fontSize: Dimens.extra_medium_gap)))),
    );
  }

  /// Method for signing in via Firebase
  /// and saving authorization state to preferences
  Future<bool> _signIn() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _emailController.text, password: _pswdController.text);
      SharedPreferences prefs = await getIt.getAsync<SharedPreferences>();
      await prefs.setBool(prefsKeyIsAuthorized, true);
      return true;
    } on FirebaseAuthException catch (e) {
      if (e.code == errorCodeUserNotFound) {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text(S.of(context).error_auth_user_not_found),
        ));
      } else if (e.code == errorCodeWrongPassword) {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text(S.of(context).error_auth_wrong_password),
        ));
      }
      print(e);
      return false;
    }
  }

  void _navigateToMain() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MenuScreen()));
  }

  Widget _registrationButton() {
    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: EdgeInsets.symmetric(
            horizontal: Dimens.medium_gap, vertical: Dimens.small_gap),
        child: OutlinedButton(
            onPressed: () => _navigateToRegistration(),
            child: Text(S.of(context).registration_button,
                style: TextStyle(fontSize: Dimens.extra_medium_gap))),
      ),
    );
  }

  void _navigateToRegistration() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => RegistrationScreen()));
  }
}
