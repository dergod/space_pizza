import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:space_pizza/data/network/NetworkManager.dart';

/// Global service locator instance
/// for getting any objects
final getIt = GetIt.instance;

/// Initializing method
void setupDi() {
  /* Shared Preferences */
  getIt.registerLazySingletonAsync<SharedPreferences>(
      () async => SharedPreferences.getInstance());

  /* Firebase */
  getIt.registerLazySingletonAsync<FirebaseApp>(
      () async => Firebase.initializeApp());
  getIt.registerFactory<FirebaseAuth>(() => FirebaseAuth.instance);

  /* Network */
  getIt.registerLazySingleton<Logger>(() => Logger());
  getIt.registerLazySingleton<RestClient>(() {
    return RestClient(Dio());
  });
}
