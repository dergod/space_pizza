/// This is the file which contains all app dimensions

class Dimens {
  static const small_gap = 8.0;
  static const medium_gap = 16.0;
  static const extra_medium_gap = 18.0;
  static const large_gap = 24.0;
}
