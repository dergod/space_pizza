import 'package:json_annotation/json_annotation.dart';

part 'PizzaModel.g.dart';

/// Model with pizza data
@JsonSerializable()
class PizzaModel {
  String id;
  String name;
  String avatar;

  PizzaModel({this.id, this.name, this.avatar});

  factory PizzaModel.fromJson(Map<String, dynamic> json) =>
      _$PizzaModelFromJson(json);

  Map<String, dynamic> toJson() => _$PizzaModelToJson(this);
}
