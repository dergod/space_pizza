// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PizzaModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PizzaModel _$PizzaModelFromJson(Map<String, dynamic> json) {
  return PizzaModel(
    id: json['id'] as String,
    name: json['name'] as String,
    avatar: json['avatar'] as String,
  );
}

Map<String, dynamic> _$PizzaModelToJson(PizzaModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'avatar': instance.avatar,
    };
