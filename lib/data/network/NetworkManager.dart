import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

import 'package:space_pizza/data/model/PizzaModel.dart';

part 'NetworkManager.g.dart';

/// Main class for working with network requests
@RestApi(baseUrl: "https://5d42a6e2bc64f90014a56ca0.mockapi.io/api/v1/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/pizzas")
  Future<List<PizzaModel>> getPizzaMenu();
}
