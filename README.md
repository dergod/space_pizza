# space_pizza

Pizza ordering app

## Technologies
- Flutter
- Dart
- Network: Retrofit/Dio
- DI: GetIt
- Firebase Authentication
- Internalization